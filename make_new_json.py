import pandas as pd
import numpy as np
import scipy as sp
from scipy import linalg
import matplotlib.pyplot as plt
import seaborn as sns
import json
from html import unescape

#raw_features = pd.read_csv("characters_raw/aggregated-means.csv", sep="\t", low_memory=False)
raw_features = pd.read_csv("characters_raw/aggregated-n.csv", sep="\t", low_memory=False)
#print(raw_features.head())
#print(raw_features.columns)

f = open("characters_raw/measures.json")
feature_names = json.load(f)

#235 - 266 are the emojis and 333 - 335
emojis = {"F"+str(i):feature_names[str(i)][0] + " - " + feature_names[str(i)][1] for i in range(235,267)}
more_emojis = {"F"+str(i):feature_names[str(i)][0] + " - " + feature_names[str(i)][1] for i in range(333,336)}

column_mapping = {"F"+str(i):feature_names[str(i)][0] + " - " + feature_names[str(i)][1] for i in range(1,401)}
#98 and 183 are both "hard - soft"
column_mapping["F183"] = "hard - soft 2"
#print(column_mapping)
raw_features.rename(columns=column_mapping, inplace=True)

g = open("characters_raw/subjects.json")
character_names = json.load(g)

character_name_mapping = {}
name_only_mapping = {}
universe_only_mapping = {}
for character in raw_features["Unnamed: 0"]:
    abbrev = character.split("/")
    universe = abbrev[0]
    which_character = abbrev[1]
    character_name_mapping[character] = unescape(character_names[universe]["name"]) + ": " + unescape(character_names[universe][which_character][0])
    name_only_mapping[character] = unescape(character_names[universe]["name"])
    universe_only_mapping[character] = unescape(character_names[universe][which_character][0])

#print(character_name_mapping)
# note name and universe are backwards in the names of the dictionaries
raw_features['Name'] = raw_features['Unnamed: 0'].map(universe_only_mapping)
raw_features['Universe'] = raw_features['Unnamed: 0'].map(name_only_mapping)
raw_features = raw_features.replace({"Unnamed: 0": character_name_mapping})
raw_features.rename(columns={"Unnamed: 0": "Universe: Name"}, inplace=True)
raw_features = raw_features.drop(columns = emojis.values())
raw_features = raw_features.drop(columns = more_emojis.values())

#print(raw_features.shape)
raw_features_json = raw_features.to_json("characters_x_traits_n.json")
#raw_features.to_csv("characters_x_traits.csv")